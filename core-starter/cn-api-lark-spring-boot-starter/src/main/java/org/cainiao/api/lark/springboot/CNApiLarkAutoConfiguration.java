package org.cainiao.api.lark.springboot;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Import;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@AutoConfiguration
@ConditionalOnProperty(name = "cn.api.lark.enable", havingValue = "true", matchIfMissing = true)
@Import({
    CNWebClientImperativeApiLarkConfiguration.class
})
public class CNApiLarkAutoConfiguration {
}
